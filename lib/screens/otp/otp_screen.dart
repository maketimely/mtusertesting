import 'package:flutter/material.dart';
import 'package:mt_user/screens/login/components/body.dart';

class OtpScreen extends StatelessWidget {
  const OtpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(11, 115, 233, 1),
      body: Body(),
    );
  }
}
