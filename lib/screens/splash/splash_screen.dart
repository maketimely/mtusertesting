import 'dart:async';
import 'package:flutter/material.dart';
import 'package:mt_user/screens/login/login_screen.dart';

class SplashScreen extends StatefulWidget {
  static String routeName = "/splash";
  // final String text;
  // const SplashScreen({
  //  Key? key,
  //   required this.text,
  // }) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 2), () {
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (_) => LoginScreen()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(11, 115, 233, 1),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: Center(
              child: Container(
                width: 125,
                height: 125,
                decoration: BoxDecoration(
                    image: DecorationImage(
                  image: AssetImage("assets/img/mt.png"),
                )),
              ),
            ),
          ),
          Container(
            child: Center(
              child: Text(
                'maketimley',
                style: TextStyle(
                  fontFamily: 'Metropolis',
                  fontWeight: FontWeight.w500,
                  fontSize: 30,
                  color: Colors.white,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
