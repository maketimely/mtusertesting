import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mt_user/components/defaultbutton.dart';

import 'package:mt_user/routes/router.dart' as route;
import 'package:mt_user/screens/otp/otp_screen.dart';

class Body extends StatefulWidget {
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: Column(
        children: [
          Expanded(
            flex: 6,
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 36.0, horizontal: 24),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SvgPicture.asset(
                    "assets/icons/login-illus-2.svg",
                    height: 200,
                    width: 300,
                  ),
                  SizedBox(height: size.height * 0.03),
                  Text(
                    "Welcome to mt",
                    style: TextStyle(
                      fontFamily: 'Metropolis',
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                      fontSize: 22,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Flexible(
            flex: 3,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8),
                    topRight: Radius.circular(8),
                  )),
              child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextField(
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                        labelText: "Enter Mobile Number",
                      ),
                    ),
                    SizedBox(height: size.height * 0.02),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: TextButton(
                        child: Text("Send OTP"),
                        onPressed: () {
                          print(route.otpScreen);
                          // Navigator.of(context).pushNamed("/OtpScreen");
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => OtpScreen()));
                        },
                        // text: ('Send OTP'),
                        // press: () {
                        //   print("clocked\n");
                        //   Navigator.of(context).pushNamed(route.otpScreen);
                        // },
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Flexible(
            flex: 2,
            child: Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: Colors.lightBlue[100],
              ),
              child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "by logging in you are agreeing to",
                      style: TextStyle(
                        fontFamily: 'Metropolis',
                        fontWeight: FontWeight.w600,
                        color: Colors.black,
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(height: size.height * 0.02),
                    Text(
                      "Our Terms of Service & Privacy Policy",
                      style: TextStyle(
                        fontFamily: 'Metropolis',
                        fontWeight: FontWeight.w600,
                        color: Colors.black38,
                        fontSize: 10,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

// import 'package:flutter/material.dart';
// import 'package:flutter_svg/svg.dart';

// class Body extends StatelessWidget {
//   const Body({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Column(
//         children: <Widget>[
//           Flexible(
//               flex: 6,
//               child: Container(
//                 child: SvgPicture.asset(
//                   "assets/icons/login-illus-1.svg",
//                   height: 250,
//                   width: 250,
//                 ),
//               )),
//           Flexible(
//             flex: 3,
//             child: Container(
//               color: Colors.white,
//               child: TextField(),
//             ),
//           ),
//           Flexible(
//             flex: 1,
//             child: Container(
//               color: Color.fromRGBO(191, 243, 255, 1),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
