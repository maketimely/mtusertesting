import 'package:flutter/material.dart';
import 'package:mt_user/screens/login/components/body.dart';

class LoginScreen extends StatelessWidget {
  static String routeName = "/login";

  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(),
      backgroundColor: Color.fromRGBO(11, 115, 233, 1),
    );
  }
}
