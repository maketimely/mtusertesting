// // GENERATED CODE - DO NOT MODIFY BY HAND

// // **************************************************************************
// // AutoRouteGenerator
// // **************************************************************************

// // ignore_for_file: public_member_api_docs

// import 'package:auto_route/auto_route.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:mt_user/screens/otp/otp_screen.dart';
// import 'package:mt_user/screens/welcome/welcome.dart';
// import 'package:mt_user/screens/login/login_screen.dart';
// import 'package:mt_user/screens/splash/splash_screen.dart';

// class Routes {
//   static const String splashScreen = '/';
//   static const String welcome = '/welcome';
//   static const String loginScreen = '/login-screen';
//   static const String otpScreen = '/otp-screen';
//   static const all = <String>{
//     splashScreen,
//     welcome,
//     loginScreen,
//     otpScreen,

//   };
// }

// class AppRouter extends RouterBase {
//   @override
//   List<RouteDef> get routes => _routes;
//   final _routes = <RouteDef>[
//     RouteDef(Routes.splashScreen, page: SplashScreen),
//     RouteDef(Routes.welcome, page: Welcome),
//     RouteDef(Routes.loginScreen, page: LoginScreen),
//     RouteDef(Routes.otpScreen, page: OtpScreen),
//   ];
//   @override
//   Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
//   final _pagesMap = <Type, AutoRouteFactory>{
//     SplashScreen: (data) {
//       return CupertinoPageRoute<dynamic>(
//         builder: (context) => SplashScreen(),
//         settings: data,
//       );
//     },

//     Welcome: (data) {
//       return CupertinoPageRoute<dynamic>(
//         builder: (context) => Welcome(),
//         settings: data,
//       );
//     },
//     LoginScreen: (data) {
//       return CupertinoPageRoute<dynamic>(
//         builder: (context) => LoginScreen(),
//         settings: data,
//       );
//     },
//     OtpScreen: (data) {
//       final args = data.getArgs<OtpScreenArguments>(
//         orElse: () => OtpScreenArguments(),
//       );
//       return CupertinoPageRoute<dynamic>(
//         builder: (context) => OtpLoginScreen(phoneNumber: args.phoneNumber),
//         settings: data,
//       );
//     },

//   };
// }

// /// ************************************************************************
// /// Arguments holder classes
// /// *************************************************************************
// /// CheckStatusScreen arguments holder class
// // class CheckStatusScreenArguments {
// //   final bool checkForAccountStatusOnly;
// //   CheckStatusScreenArguments({this.checkForAccountStatusOnly = false});
// // }

// // /// OtpLoginScreen arguments holder class
// // class OtpLoginScreenArguments {
// //   final String phoneNumber;
// //   OtpLoginScreenArguments({this.phoneNumber});
// // }

// // /// ProductDetailPage arguments holder class
// // class ProductDetailPageArguments {
// //   final ProductModel productModel;
// //   ProductDetailPageArguments({@required this.productModel});
// // }

// // /// AddUserDetailScreen arguments holder class
// // class AddUserDetailScreenArguments {
// //   final bool newAddress;
// //   AddUserDetailScreenArguments({@required this.newAddress});
// // }

// // /// AllProductListScreen arguments holder class
// // class AllProductListScreenArguments {
// //   final String productCondition;
// //   AllProductListScreenArguments({this.productCondition});
// // }

// // /// MyAddressScreen arguments holder class
// // class MyAddressScreenArguments {
// //   final bool selectedAddress;
// //   MyAddressScreenArguments({this.selectedAddress = false});
// // }

// // /// AddAddressScreen arguments holder class
// // class AddAddressScreenArguments {
// //   final bool newAddress;
// //   final AccountDetails accountDetails;
// //   final Address editAddress;
// //   AddAddressScreenArguments(
// //       {@required this.newAddress,
// //       @required this.accountDetails,
// //       this.editAddress});
// // }
