import 'package:flutter/material.dart';
import 'package:mt_user/screens/login/login_screen.dart';
import 'package:mt_user/screens/otp/otp_screen.dart';
import 'package:mt_user/screens/splash/splash_screen.dart';
import 'package:mt_user/screens/welcome/welcome.dart';

const String splashScreen = 'SplashStocreen';
const String loginScreen = 'LoginScreen';
const String welcome = 'Welcome';
const String otpScreen = 'OtpScreen';

Route<dynamic> controller(RouteSettings settings) {
  switch (settings.name) {
    case splashScreen:
      return MaterialPageRoute(builder: (context) => SplashScreen());
    case loginScreen:
      return MaterialPageRoute(builder: (context) => LoginScreen());
    case welcome:
      return MaterialPageRoute(builder: (context) => Welcome());
    case otpScreen:
      return MaterialPageRoute(builder: (context) => OtpScreen());
    default:
      throw ('this route name does not exit');
  }
}
