import 'package:flutter/material.dart';
import 'package:mt_user/screens/login/login_screen.dart';
import 'package:mt_user/screens/otp/otp_screen.dart';
import 'package:mt_user/screens/splash/splash_screen.dart';
import 'package:mt_user/screens/welcome/welcome.dart';

const String splashScreen = '/SplashStocreen';
const String loginScreen = '/LoginScreen';
const String welcome = '/Welcome';
const String otpScreen = '/OtpScreen';

Route<dynamic> controller(RouteSettings settings) {
  switch (settings.name) {
    case splashScreen:
      return MaterialPageRoute(
          builder: (context) => SplashScreen(), settings: settings);
    case loginScreen:
      return MaterialPageRoute(
          builder: (context) => LoginScreen(), settings: settings);
    case welcome:
      return MaterialPageRoute(
          builder: (context) => Welcome(), settings: settings);
    case otpScreen:
      return MaterialPageRoute(
          builder: (context) => OtpScreen(), settings: settings);
    default:
      throw ('this route name does not exit');
  }
}
