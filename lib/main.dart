import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mt_user/constants.dart';
import 'package:mt_user/routes/router.dart' as route;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'maketimely',
      theme: ThemeData(
        primaryColor: kPrimaryColor,
      ),
      home: SplashScreen(),

      // initialRoute: Routes.splashScreen,
      // home: SplashScreen(),
      // routes: {
      //   LoginScreen.routeName: (context) => LoginScreen(),

      // },
    );
  }
}
