import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xff454545);
const kPrimaryLightColor = Color(0xff454545);
const kPrimarySwatch = Color(0xff454545);
